package utils.types

/**
 * An IPv4.
 *
 * @param _1 The first byte of the address
 * @param _2 The second byte of the address
 * @param _3 The third byte of the address
 * @param _4 The last byte of the address
 */
class Ipv4(_1: Int, _2: Int, _3: Int, _4: Int) {
  override def toString: String = f"IP: ${_1}.${_2}.${_3}.${_4}"
}

object Ipv4 {

  /**
   * Parse a string and return an `Ipv4`.
   *
   * @param ipStr The ip as a string. ex: 10.0.0.1
   * @return An Ipv4 Object
   */
  def fromString(ipStr: String): Ipv4 = {
    val ip = ipStr.split("\\.").map(_.toInt)
    val msg = f"The IP \'$ip\' is not a valid IPv4 address."

    // Check IP validity
    require(ip.length == 4, msg)
    ip.foreach( n => if (n < 0 || n > 255) throw new IllegalArgumentException(msg))

    new Ipv4(ip(0),ip(1),ip(2),ip(3))
  }
}
