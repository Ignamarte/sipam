package utils.config

import errors.ConfigError
import pureconfig.ConfigSource
import pureconfig.generic.auto._

/**
 * The singleton object loading the API configuration file.
 */
object APIConfig {

  case class API(
                  loginEndpoint: String,
                  sectionEndpoint: String,
                  subnetEndpoint: String,
                  addressEndpoint: String
                )

  case class Config(api: API)

  private val config = ConfigSource.default.load[Config] match {
    case Right(c) => c
    case Left(e) => throw new ConfigError(e.toString)
  }

  def getLoginEndpoint: String = config.api.loginEndpoint
  def getSectionEndpoint: String = config.api.sectionEndpoint
  def getSubnetEndpoint: String = config.api.subnetEndpoint
  def getAddressEndpoint: String = config.api.addressEndpoint
}
