package utils.config

import java.io.PrintWriter
import java.nio.file.{Files, Path, Paths}

import errors.APIConfigError
import pureconfig.{ConfigSource, ConfigWriter}
import pureconfig.generic.auto._

/**
 * Object managing the user configuration file.
 */
object UserConfig {

  private case class Config (
                    token: Option[String] = None,
                    verifySsl: Boolean = true,
                    apiUrl: Option[String] = None,
                    appName: Option[String] = None,
                    height: Option[Double] = None,
                    width: Option[Double] = None,
                    maximized: Option[Boolean] = None
                    )

  private val file = "user.conf"
  private val path: Path = Paths.get(file)

  // Load the configuration
  private var conf: Config = if (Files.exists(path)) {
    ConfigSource.default(ConfigSource.file(path)).load[Config] match {
      case Right(c) => c
      case Left(e) => throw new APIConfigError(e.toString)
    }
  } else {
    Config()
  }

  private[utils] def getToken: Option[String] = {
    conf.token
  }

  def getApiUrl: Option[String] = {
    conf.apiUrl
  }

  def getAppName: Option[String] = {
    conf.appName
  }

  def getVerifySsl: Boolean = {
    conf.verifySsl
  }

  def getHeight: Option[Double] = {
    conf.height
  }

  def getWidth: Option[Double] = {
    conf.width
  }

  def getIsMaximized: Option[Boolean] = {
    conf.maximized
  }

  def setToken(token: Option[String]): Unit = {
    conf = conf.copy(token = token)
    saveConf()
  }

  def setToken(token: String): Unit = {
    conf = conf.copy(token = Some(token))
    saveConf()
  }

  def setApiUrl(url: String): Unit = {
    conf = conf.copy(apiUrl = Some(url))
    saveConf()
  }

  def setAppName(name: String): Unit = {
    conf = conf.copy(appName = Some(name))
    saveConf()
  }

  def setVerifySsl(verifySsl: Boolean): Unit = {
    conf = conf.copy(verifySsl = verifySsl)
    saveConf()
  }

  def setHeight(height: Double): Unit = {
    conf = conf.copy(height = Some(height))
    saveConf()
  }

  def setWidth(width: Double): Unit = {
    conf = conf.copy(width= Some(width))
    saveConf()
  }

  def setMaximized(maximized: Boolean): Unit = {
    conf = conf.copy(maximized = Some(maximized))
    saveConf()
  }

  private def saveConf(): Unit = {
    val writer = ConfigWriter[Config].to(conf)
    val out = new PrintWriter(file)
    try {
      out.write(writer.render())
    } finally {
      out.close()
    }
  }

}
