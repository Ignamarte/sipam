package utils

import java.time.{Duration, LocalDateTime}

import api.Client
import api.objects.AuthToken
import errors.NotLoggedInError
import requests.{InvalidCertException, RequestFailedException, UnknownHostException}
import utils.config.UserConfig

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

/**
 * An object managing sessions with the API.
 */
object AuthManager {

  private var lastRefresh = LocalDateTime.now()

  /**
   * Check if the user is authenticated.
   * @return true if the user is authenticated, false otherwise.
   */
  def isAuthenticated: Future[Boolean] = Future {
    val token = UserConfig.getToken
    token match {
      //  There is no token in the config file
      case None => false
      // There is a token in the config file
      case Some(t) =>
        try {
          Client.validateToken(t)
          true // Token is valid
        }
        catch { // The request failed
          case _:RequestFailedException => false
          case _:UnknownHostException => false
          case _:InvalidCertException => false
          case e:Throwable => throw e // unexpected failure, throw it
      }
    }
  }

  /**
   * Send an authentication request and store the result.
   *
   * @param user The username
   * @param passwd The password
   * @return Whether the authentication succeeded and a message if it didn't.
   */
  def authenticate(user: String, passwd: String):
  Future[(Boolean, Option[String])] = Future {
    try {
      val authToken = AuthToken.getAuthToken(user, passwd)
      UserConfig.setToken(authToken.token)
      (true, None)
    } catch {
      case e:RequestFailedException => (false, Some(e.response.text))
      case e:UnknownHostException => (false, Some(s"Host ${e.host} not found."))
      case _:InvalidCertException => (false, Some(s"SSL verification failed"))
      case e: Throwable => throw e
    }
  }

  /**
   * The method should be **the only way to retrieve the auth token**.
   * It refreshes the token if needed, and throw an error if the user is not
   * logged in.
   *
   * @return A valid auth token
   */
  def refreshAndGetToken(): String = {
    val token = UserConfig.getToken.getOrElse(throw new NotLoggedInError)
    val delta = Duration.between(lastRefresh,LocalDateTime.now)
    // validate token if it's more than 60 minutes old
    if (delta.toMinutes > 60) {
      if (Await.result(isAuthenticated, 10.seconds)) {
          Client.refreshToken(token)
          lastRefresh = LocalDateTime.now
          token
      } else throw new NotLoggedInError
    } else token
  }
}
