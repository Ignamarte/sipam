package gui

import scalafx.scene.control.{Menu, MenuBar, MenuItem}
import scalafx.scene.input.{KeyCode, KeyCodeCombination, KeyCombination}

/**
 * The Top Menu for the main window.
 */
object TopMenu extends MenuBar{

  /* File Category */
  private val files: Menu = new Menu("File") {

    private val open = new MenuItem("Open"){
      accelerator = new KeyCodeCombination(KeyCode.O, KeyCombination.ControlDown)
    }

    private val save = new MenuItem("Save"){
      accelerator = new KeyCodeCombination(KeyCode.S, KeyCombination.ControlDown)
    }

    /* Closes the window */
    private val exit = new MenuItem("Exit"){
      accelerator = new KeyCodeCombination(KeyCode.Q, KeyCombination.ControlDown)
      onAction = _ => MainStage.close()
    }

    items = List[MenuItem](open, save, exit)
  }

  /* Edit Category */
  private val edit = new Menu("Edit")

  /* Help Category */
  private val help = new Menu("Help")

  menus = List[Menu](
    files,
    edit,
    help
  )
}
