package gui.panel

import gui.dialog.ErrorDialog
import scalafx.application.Platform
import utils.AuthManager

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

/**
 * A simple trait which checks if the user is authenticated and throw an
 * error if not.
 */
trait LoginRequired {
  /*
   * Callback to implement in order to display the panel after checking the
   * authentication.
   */
  def initialize(): Unit
  // Async task to check user authentication.
  AuthManager.isAuthenticated onComplete {
    case Success(true) => Platform.runLater(initialize())
    case Success(false) => Platform.runLater(MainPanel.displayLoginPanel())
    case Failure(e) => Platform.runLater({
      MainPanel.displayLoginPanel()
      new ErrorDialog(e.getMessage, Some(e.getStackTrace.mkString("\n")))
    })
  }
}
