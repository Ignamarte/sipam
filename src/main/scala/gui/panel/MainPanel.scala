package gui.panel

import scalafx.scene.Node
import scalafx.scene.layout.AnchorPane

/**
 * The Main Panel.
 * It exposes methods to display other panel in the center of the main window.
 *
 */
object MainPanel extends AnchorPane {

  /* Display a node */
  private def displayNode(node: Node): Unit = {
    children = node
    AnchorPane.setLeftAnchor(node, 0)
    AnchorPane.setRightAnchor(node, 0)
    AnchorPane.setTopAnchor(node, 0)
    AnchorPane.setBottomAnchor(node, 0)
  }

  /* Display a specific Ipam Panel */
  def displayIpamPanel(): Unit = {
    displayNode(new IpamPanel)
  }

  def displayLoginPanel(callback: () => Unit = displayIpamPanel): Unit = {
    displayNode(new LoginPanel(callback))
  }

}
