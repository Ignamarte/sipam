package gui.panel

import animatefx.animation.FadeIn
import api.objects.collections.{Sections, Subnets}
import api.objects.{Section, Subnet}
import com.panemu.tiwulfx.control._
import gui.dialog.ErrorDialog
import gui.label.SectionLabel
import gui.widget.{SubnetTab, WelcomeTab}
import scalafx.Includes._
import scalafx.application.Platform
import scalafx.collections.ObservableBuffer
import scalafx.geometry.Pos.TopCenter
import scalafx.scene.control._
import scalafx.scene.input.{ClipboardContent, DragEvent, TransferMode}
import scalafx.scene.layout.Priority.Always
import scalafx.scene.layout.{AnchorPane, VBox}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

class IpamPanel extends SplitPane with LoginRequired {

  /* Various components */
  private val progress = new ProgressIndicator()
  private val progressBar = new ProgressBar()

  private val sideMenu = new VBox(){
    styleClass.add("side-menu")
    alignment = TopCenter
    children.add(progress)
  }


  /* Special type to include the root node which is not a subnet but a section
  * This introduces a bit of boilerplate such as pattern matching here and
  * there, but that's still better than creating a fake Subnet with the
  * Section's name */
  private type treeType = Either[Section, Subnet]

  /* Cell factory, responsible for handling drag events */
  private val treeFactory = (_: TreeView[treeType]) => new TreeCell[treeType](){

    item.onChange {(_,_,value: treeType) =>
      text = value match {
        case Left(section) => section.name
        case Right(subnet) => subnet.description.getOrElse(subnet.subnet)
        case _ => ""
      }
    }

    onDragDetected = _ => {
      // Only allow the user to drag subnets
      item.value match {
        case Right(subnet) =>
          val dragBoard = startDragAndDrop(TransferMode.Move)
          val content = new ClipboardContent()
          content.putString(subnet.id)
          dragBoard.setContent(content)
          dragBoard.setDragView(snapshot(null, null))
        case _ =>
      }
    }
  }

  // The sidemenu's tree
  private val subnetTree = new TreeView[treeType](){
    vgrow = Always
    cellFactory = treeFactory
  }

  // The Container of the detachable Tabs
  private val tabPane = new DetachableTabPane(){
    getTabs.add(WelcomeTab)
    // Accept dragged elements
    onDragOver = (e: DragEvent) => e.acceptTransferModes(TransferMode.Move)
    // When a tab is dropped on the splitpane.
    onDragDropped = (e: DragEvent) => {
      val id = e.dragboard.string
      getTabs.add(new SubnetTab(id))
    }
  }

  /* Put the DetachableTabPane in a dedicated SplitPane to prevent it from
   * messing with the current SplitPane */
  private val centerPanelContainer: SplitPane = new SplitPane() {
    items.add(tabPane)
  }
  private val splitPaneWrapper = new AnchorPane(){
    children = centerPanelContainer
    AnchorPane.setLeftAnchor(centerPanelContainer, 0)
    AnchorPane.setRightAnchor(centerPanelContainer, 0)
    AnchorPane.setTopAnchor(centerPanelContainer, 0)
    AnchorPane.setBottomAnchor(centerPanelContainer, 0)
  }

  // Fade animation used multiple times
  private val fadeAnimation = new FadeIn(sideMenu)

  /**
   * Initialize the panel
   */
   override def initialize(): Unit = {
     items.addAll(sideMenu, splitPaneWrapper)
     setDividerPosition(0, 0.2f)
     SplitPane.setResizableWithParent(sideMenu, value = false)
     fadeAnimation.play()
     Sections.getSections onComplete {
       case Success(sections) => Platform.runLater(displaySectionsChoice(sections))
       case Failure(exception) => new ErrorDialog(exception)
     }
  }

  /**
   * Display a choice box to select an IPAM section
   *
   * @param sections A list of `Section`
   */
  private def displaySectionsChoice(sections: List[Section]): Unit = {
    val sectionNameIdMap = sections.map(s => s.name -> s).toMap
    val sectionChoiceBox = new ChoiceBox[String]() {
      prefWidth <== sideMenu.width
      items = ObservableBuffer(sectionNameIdMap.keys.toList)
      /*
      * Action when a new section is selected
      */
      onAction = _ => {
        sideMenu.children.remove(progressBar)
        sideMenu.children.add(progressBar)
        val section = sectionNameIdMap(value.value)
        Subnets.getSectionSubnets(section.id) onComplete {
          case Success(subnets) => createTree(section, subnets)
          case Failure(exception) => new ErrorDialog(exception)
        }
      }
    }
    /* Update the Menu */
    sideMenu.children.remove(progress)
    sideMenu.children.addAll(
      new SectionLabel("IPAM Section"),
      sectionChoiceBox,
      new Separator(),
      subnetTree
    )
    fadeAnimation.play()
  }

  /**
   * Create the treeView content based on a provided section and its subnets.
   *
   * @param section The currently selected section
   * @param subnets The subnets of the section
   */
  private def createTree(section: Section, subnets: List[Subnet]): Unit = {

    /**
     * Create a subnet `TreeItem` based on the provided subnet
     *
     * @param subnet The subnet
     * @return A `TreeItem` object
     */
    def createTreeItem(subnet: Subnet, subGroups: Map[String, List[Subnet]]):
    TreeItem[treeType] = {
      val slaves = subGroups.getOrElse(subnet.id, List())
      new TreeItem[treeType](Right(subnet)){
        slaves.foreach(s => children.add(createTreeItem(s, subGroups)))
      }
    }

    val root = new TreeItem[treeType](Left(section)){expanded = true}
    val subnetsGroup = subnets.groupBy(s => s.masterSubnetId)
    val masterSubnets = subnetsGroup.getOrElse("0", List())
    masterSubnets.foreach(s => root.children.add(createTreeItem(s,subnetsGroup)))

    /* Display the subnets tree */
    Platform.runLater({
      sideMenu.children.remove(progressBar)
      subnetTree.root = root
    })
  }
}
