package gui.panel

import gui.dialog.ErrorDialog
import gui.label.{SectionLabel, TitleLabel}
import scalafx.Includes._
import scalafx.application.Platform
import scalafx.geometry.Insets
import scalafx.geometry.Pos.{BaselineRight, TopCenter}
import scalafx.scene.control._
import scalafx.scene.input.{KeyCode, KeyEvent}
import scalafx.scene.layout.GridPane

import scala.concurrent.ExecutionContext.Implicits.global
import utils.AuthManager
import utils.config.UserConfig

import scala.util.{Failure, Success}


/**
 * The login panel
 *
 * @param callback: The action to undergo after a successful login.
 */
class LoginPanel(callback: () => Unit) extends GridPane{

  private val loader = new ProgressIndicator()

  /* Title */
  private val title = new TitleLabel("Login")

  /* API URL */
  private val apiUrlLabel = new Label("API URL ")
  private val apiUrlField = new TextField(){
    text = UserConfig.getApiUrl.getOrElse("")
    promptText = "API URL"
  }

  /* Verify SSL */
  private val verifySslLabel = new Label("Check SSL certificate ")
  private val verifySslcheckBox = new CheckBox(){
    selected = UserConfig.getVerifySsl
  }

  /* API APP Name */
  private val appNameLabel = new Label("API App name ")
  private val appNameField = new TextField(){
    text = UserConfig.getAppName.getOrElse("")
    promptText = "App Name"
  }

  /* Credential section title */
  private val credentialSection = new SectionLabel("Credentials")

  /* Credentials */
  private val usernameLabel = new Label("Username ")
  private val usernameField = new TextField(){
    promptText = "Username"
  }

  private val passwordLabel = new Label("Password ")
  private val passwordField = new PasswordField(){
    promptText = "Password"
    onKeyPressed = (e:KeyEvent) => if(e.code == KeyCode.Enter) startLogin()
  }

  private val loginButton = new Button(){
    text = "Login"
    alignmentInParent = BaselineRight
    /* Start the authentication process when clicked */
    onMouseClicked = _ => startLogin()
  }

  /* Style */
  alignment = TopCenter
  padding = Insets(20, 100, 10, 10)
  vgap = 20
  hgap = 20

  /* Add elements to the grid */
  add(title, 0, 0, 4, 1)

  add(apiUrlLabel, 0, 1)
  add(apiUrlField, 1, 1)
  add(verifySslLabel, 2,1)
  add(verifySslcheckBox, 3,1)

  add(appNameLabel, 0,2)
  add(appNameField, 1,2)

  add(credentialSection, 0, 3, 4,1)

  add(usernameLabel, 0,4)
  add(usernameField, 1,4)

  add(passwordLabel, 0,5)
  add(passwordField, 1,5)


  /* Wrap button positioning in a small function for further reuse */
  private def addLoginButton(): Unit = add(loginButton,1,6)
  addLoginButton()

  /* Start login sequence */
  private def startLogin(): Unit = {
    if (checkUrl()) {
      toggleLoader()
      saveNewConfig()
      // Async task t avoid freezing the GUI
      val resultFuture = AuthManager.authenticate(
        usernameField.text.value,
        passwordField.text.value
      )
      resultFuture onComplete {
        case Success((succeeded, msg)) =>
          Platform.runLater(postLogin(succeeded, msg))
        case Failure(e) => Platform.runLater({
            toggleLoader(hide = true)
            new ErrorDialog(e.getMessage, Some(e.getStackTrace.mkString("\n")))
          })
      }
    }
  }

  /**
   * Check if the url provided is somewhat valid
   *
   * @return Whether the url is valid or not
   */
  private def checkUrl(): Boolean = {
    val httpr = "^(https?://)".r
    val url = apiUrlField.text.value

    val errorMsg =
      if (httpr.findFirstMatchIn(url).isEmpty) {
        val text = "should include a protocol such as http or https."
        Some(s"${appNameLabel.text.value.strip()} $text")
      }
      else None

    /* Display the error */
    if (errorMsg.isDefined) {
      new ErrorDialog(errorMsg.get)
      false
    } else {
      true
    }
  }

  /**
   * Save the new configuration
   */
  private def saveNewConfig(): Unit = {
    /* Save the new configuration */
    UserConfig.setApiUrl(apiUrlField.text.value.stripSuffix("/"))
    UserConfig.setAppName(appNameField.text.value)
    UserConfig.setVerifySsl(verifySslcheckBox.selected.value)
  }

  /**
   * Call the login callback such as displaying another panel if the login
   * process succeed.
   * If the process failed, an error is displayed.
   *
   * @param success Whether the login was successful
   * @param msg An optional error message
   */
  def postLogin(success: Boolean, msg: Option[String]): Unit ={
    toggleLoader(hide = true)

    val defaultMsg = "No error message returned, this is not supposed to happen"

    if (success)
      callback()
    else
      new ErrorDialog(msg.getOrElse(defaultMsg))
  }

  /**
   * Display or add the loading indicator.
   * @param hide Whether to hide or display the loader
   */
  def toggleLoader(hide: Boolean = false): Unit ={
    if (hide) children.remove(loader)
    else try{
      add(loader, 2, 4,2,2)
    } catch {
      case e:IllegalArgumentException => // Already displayed
      case e:Throwable => throw e
    }
  }
}
