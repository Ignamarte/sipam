package gui.dialog

import gui.MainStage
import scalafx.scene.control.{Alert, Label}
import scalafx.scene.control.Alert.AlertType

/**
 * An error dialog with an expendable section.
 *
 * @param shortMsg The main message
 * @param msg The message displayed in the expandable section
 */
class ErrorDialog(shortMsg: String, msg: Option[String] = None)
  extends Alert(AlertType.Error){

  def this(e: Throwable) = this(e.getMessage, Some(e.getStackTrace.mkString("\n")))

  initOwner(MainStage)

  title = "ERROR"
  headerText = "Error"

  contentText = shortMsg
  if(msg.isDefined) dialogPane().setExpandableContent(new Label(msg.get))

  showAndWait()
}
