package gui.widget

import api.objects.Address
import scalafx.geometry.Pos.Center
import scalafx.scene.control.Label
import scalafx.scene.layout.{Region, VBox}

/**
 * A widget displaying info about a specific IP address
 *
 * @param address The `Address` object to display
 */
class IpWidget(address: Address) extends VBox{

  styleClass.add("ip-widget")
  alignment = Center

  private val IpLabel = new Label(address.ip) {
    styleClass.addAll("dark-blue")
  }

  private val hostname = new Label (address.hostname.getOrElse("")) {
    styleClass.addAll("bold", "dark-blue")
  }

  children.add(IpLabel)
  children.add(hostname)
}
