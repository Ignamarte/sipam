package gui.widget

import api.objects.collections.Addresses
import api.objects.{Address, Subnet}
import com.panemu.tiwulfx.control.DetachableTab
import gui.dialog.ErrorDialog
import gui.label.TitleLabel
import gui.widget.WelcomeTab.setDetachable
import scalafx.application.Platform
import scalafx.geometry.Pos.{Center, TopCenter, TopLeft}
import scalafx.scene.control.{ProgressIndicator, ScrollPane}
import scalafx.scene.layout.{FlowPane, VBox}
import javafx.scene.{layout => jfxsl}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

/**
 * A widget displaying the IPs of a section.
 *
 * @param id The ID of the section to display
 */
class SubnetTab(id: String)
  extends DetachableTab("Loading ...") {

  setDetachable(true)

  /* Container for the content of the Tab */
  val container: VBox = new VBox() {
    alignment = TopCenter
  }
  private val wrapper = new ScrollPane(){
    fitToWidth = true
    content = container
  }

  /* The grid containing the IP addresses */
  private val IpGrid = new FlowPane() {
    alignment = TopLeft
    styleClass.add("subnet-grid")
  }

  container.children.add(new ProgressIndicator)
  setContent(wrapper)

  Subnet.getSubnet(id) onComplete {
    case Success(s) => Addresses.getSubnetAddresses(s.id) onComplete {
      case Success(addrs) => Platform.runLater(displaySubnet(s, addrs))
      case Failure(exception) => Platform.runLater(new ErrorDialog(exception))
    }
    case Failure(exception) => Platform.runLater(new ErrorDialog(exception))
  }

  /**
   * Display the used IP addresses in the subnet
   *
   * @param subnet A `Subnet` object
   * @param addresses A list `Address` in the subnet
   */
  private def displaySubnet(subnet: Subnet, addresses: List[Address]): Unit = {

    def getMaxWidth(widgets: List[IpWidget]): Future[Double]  = Future {
      while(widgets.filter(a => a.width.value == 0.0d) != Nil) {
        Thread.sleep(2000)
      }
      widgets.map(a => a.width.value).max
    }

    val name = subnet.description.getOrElse(subnet.subnet)
    setText(name)

    container.children.clear()
    container.children.add(new TitleLabel(name))
    container.children.add(IpGrid)

    val IpWidgetList: List[IpWidget] = addresses.map(addr => new IpWidget(addr))
    IpWidgetList.foreach(w => IpGrid.children.add(w))

    // Set same width for every IPWidget
    Platform.runLater({
        getMaxWidth(IpWidgetList) onComplete {
          case Success(maxWidth) =>
            IpWidgetList.foreach(w => w.prefWidth = maxWidth)
          case Failure(_:UnsupportedOperationException) =>
          case Failure(e) => throw e
        }
    })

  }
}
