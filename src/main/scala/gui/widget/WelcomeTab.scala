package gui.widget

import com.panemu.tiwulfx.control.DetachableTab
import gui.label.TitleLabel
import scalafx.geometry.Pos.TopCenter
import scalafx.scene.control.Label
import scalafx.scene.layout.VBox

object WelcomeTab extends DetachableTab("Welcome") {

  setClosable(false)
  setDetachable(false)

  private val content: VBox = new VBox() {
    alignment = TopCenter

    val title = new TitleLabel("Welcome to SIPAM !")
    val description = new Label(
      """
        |To get started, select a section using the dropdown located at the top
        |of the sidemenu.
        |
        |Add a Subnet by dragging it on this Tab.
        |
        |Tabs can be rearranged by dragging them around (except for this one).
        |""".stripMargin)

    children.add(title)
    children.add(description)
  }

  setContent(content)

}
