package gui

import errors.NotLoggedInError
import gui.dialog.ErrorDialog
import gui.panel.MainPanel
import scalafx.application.JFXApp.PrimaryStage
import scalafx.application.Platform
import scalafx.scene.Scene
import scalafx.scene.image.Image
import scalafx.scene.layout.BorderPane
import utils.config.UserConfig

/**
 * The main stage for the GUI. It initiate the whole GUI.
 *
 * It also saves the window size, if it is maximized, and display unhandled
 * error in a nice notification window.
 */
object MainStage extends PrimaryStage {

  val DEFAULT_HEIGHT = 550d
  val DEFAULT_WIDTH = 800d
  minHeight = DEFAULT_HEIGHT
  minWidth = DEFAULT_WIDTH

  title.value = "Sipam"

  /* Load the state of the window from the user config file */
  height = UserConfig.getHeight.getOrElse(DEFAULT_HEIGHT)
  width = UserConfig.getWidth.getOrElse(DEFAULT_WIDTH)
  maximized = UserConfig.getIsMaximized.getOrElse(true)

  /* Window icon */
  icons.add(new Image("/images/icon.png"))

  /**
   * Function to initialize the GUI.
   */
  def init(): Unit = {
    Thread.setDefaultUncaughtExceptionHandler(displayError)
    MainPanel.displayIpamPanel()
  }

  /* Main Scene */
  scene = new Scene() {
    stylesheets.add(getClass.getResource("/sipam.css").toExternalForm)
    root = new BorderPane() {
      top = TopMenu
      center = MainPanel
    }
    init()
  }

  /*
  * Default error handler
  *
  * NotLoggedInError -> Display a login panel
  * Anything else -> Display the error in a nice popup
  *
  * */

  private def displayError(thread: Thread, error: Throwable): Unit = {
    error match {
      case _:NotLoggedInError =>
        Platform.runLater(MainPanel.displayLoginPanel())
      case e => Platform.runLater({
        new ErrorDialog(
          f"An unexpected error occurred : \n $e",
          Some(e.getStackTrace.mkString("\n"))
        )
      })
    }
  }

  /* Resize events */

  width.onChange { (_, _, newValue) =>
    if (!maximized()) UserConfig.setWidth(newValue.doubleValue())
  }

  height.onChange { (_, _, newValue) =>
    if (!maximized()) UserConfig.setHeight(newValue.doubleValue())
  }

  maximized.onChange {(_, _, _) =>
    UserConfig.setMaximized(maximized())
  }
}
