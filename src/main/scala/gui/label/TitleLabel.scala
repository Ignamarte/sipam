package gui.label

import scalafx.scene.control.Label

class TitleLabel(title: String) extends Label(title){
  styleClass.add("title")
  maxWidth = Double.MaxValue
}
