package gui.label

import scalafx.scene.control.Label

class SectionLabel(title: String) extends Label(title){
  styleClass.add("section")
  maxWidth = Double.MaxValue
}
