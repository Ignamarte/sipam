package api

import errors.APIConfigError
import requests.RequestAuth
import utils.config.{APIConfig, UserConfig}
import errors.APIConfigError
import utils.AuthManager

/**
 * A simple requests wrapper to send requests to phpipam server.
 */
object Client {

  /* Base URL */
  private def baseURL: String = UserConfig.getApiUrl.getOrElse(
    throw new APIConfigError("Server base URL is missing")
  )
  private def appName: String = UserConfig.getAppName.getOrElse(
    throw new APIConfigError("App name is missing")
  )

  /* Endpoints */
  private val loginEndpoint: String = APIConfig.getLoginEndpoint
  private val sectionEndpoint: String = APIConfig.getSectionEndpoint
  private val subnetEndpoint: String = APIConfig.getSubnetEndpoint
  private val addressEndpoint: String = APIConfig.getAddressEndpoint

  /* Timeouts */
  private val readTimeout = 10000
  private val connectTimeout = 2000

  /**
   * Send an authentication requests and retry `retry` times if it fails.
   *
   * @param user The username used for authentication
   * @param pass The password used for authentication
   * @param retry Remaining tries before throwing an exception
   * @return A tuple containing the status code and the json text returned by
   *         the API.
   */
  @scala.annotation.tailrec
  def requestAuthToken(user: String, pass: String, retry: Int = 3): (Int, String) = {
    val auth = RequestAuth.implicitBasic(user, pass)
    try {
      val r = requests.post(
        f"$baseURL/$appName/$loginEndpoint",
        auth = auth,
        verifySslCerts = UserConfig.getVerifySsl,
        readTimeout = readTimeout,
        connectTimeout = connectTimeout
      )
      (r.statusCode, r.text)
    } catch {
      case e: Throwable =>
        if (retry > 0) requestAuthToken(user, pass, retry-1)
        else throw e
    }
  }

  /**
   * Generic method to send a request to the phpipam api server.
   * The method tries to send the requests `retry` times before returning an
   * exception.
   *
   * @param endpoint The full URL the request must be send to
   * @param method The method to use (see `requestMethod` for avilable methods)
   * @param params An array of parameters to send with the quests
   * @param retry  Remaining tries before throwing an exception
   * @param token The auth token
   * @return A tuple containing the status code and the json text returned by
   *         the API.
   */
  @scala.annotation.tailrec
  private def sendRequest(
                           endpoint: String,
                           method: String,
                           token: String,
                           params: List[(String, String)],
                           retry: Int
                         ): (Int, String) = {

    val requestMethod = method match {
      case "GET" => requests.get
      case "POST" => requests.post
      case "PUT" => requests.put
      case "DELETE" => requests.delete
      case "PATCH" => requests.patch
      case _ => throw new APIConfigError(f"Invalid method: $method")
    }

    val header = Map("token" -> token)

    // Send the request and retry if the request failed and retry != 0
    try {
      val r = requestMethod(
        endpoint,
        headers = header,
        params = params,
        verifySslCerts = UserConfig.getVerifySsl,
        readTimeout = readTimeout,
        connectTimeout = connectTimeout
      )

      // RETURN VALUES
      (r.statusCode, r.text)
    }
    catch {
      case e:Throwable =>
        if (retry > 0) {
          Thread.sleep(500)
          sendRequest(endpoint,method,token,params,retry-1)
        } else throw e
    }
  }

  /**
   * Utility function to call `sendrequest` without token.
   * The token is retrieved from the AuthManager.
   */
  private def sendRequest(
                           endpoint: String,
                           method: String,
                           params: List[(String, String)] = List[(String, String)](),
                           retry: Int = 3
                         ): (Int, String) = {
    val token = AuthManager.refreshAndGetToken()
    sendRequest(endpoint,method,token,params,retry)
  }

  /**** AUTH ****/

  /**
   * Send a request to check if an auth token is still valid.
   *
   * @param token The token to validate
   * @return
   */
  def validateToken(token: String):(Int, String) = {
    val endpoint = f"$baseURL/$appName/$loginEndpoint"
    val params = List[(String, String)]()
    sendRequest(endpoint, method="GET", token, params, retry = 1)
  }

  /**
   * Send a request to refresh a valid auth token.
   *
   * @param token The valid auth token to refresh
   * @return
   */
  def refreshToken(token: String): (Int, String) = {
    val endpoint = f"$baseURL/$appName/$loginEndpoint"
    val params = List[(String, String)]()
    sendRequest(endpoint, method="PATCH", token, params, retry = 1)
  }

  /**** SECTIONS ****/

  /**
   * Send a request to the api to retrieve every section.
   *
   * @return A tuple containing the status code and the returned json
   */
  def getSections: (Int, String) = {
    val endpoint = f"$baseURL/$appName/$sectionEndpoint"
    sendRequest(endpoint, method = "GET")
  }

  /**
   * Send a request to the api to retrieve a specific section.
   *
   * @param id The unique ID of the section
   * @return A tuple containing the status code and the returned json
   */
  def getSection(id: String): (Int, String) =
  {
    val endpoint = f"$baseURL/$appName/$sectionEndpoint/$id/"
    sendRequest(endpoint, method= "GET")
  }

  /**** SUBNETS ****/

  /**
   * Send a request to the api to retrieve every subnet of a specific section.
   *
   * @param id The unique ID of the section
   * @return A tuple containing the status code and the returned json
   */
  def getSubnetsFromSection(id: String): (Int, String) = {
    val endpoint = f"$baseURL/$appName/$sectionEndpoint/$id/$subnetEndpoint"
    sendRequest(endpoint, method="GET")
  }

  /**
   * Send a request to the api to retrieve a specific subnet with unique `id`.
   *
   * @param id The unique ID of the subnet
   * @return A tuple containing the status code and the returned json
   */
  def getSubnet(id: String): (Int, String) = {
    val endpoint = f"$baseURL/$appName/$subnetEndpoint/$id"
    sendRequest(endpoint, method="GET")
  }

  /**** ADDRESSES ****/

  /**
   * Send a request to the api to retrieve every address with unique of a
   * specific subnet with unique`id`.
   *
   * @param id The unique ID of the subnet
   * @return A tuple containing the status code and the returned json
   */
  def getAddressesFromSubnet(id: String): (Int, String) = {
    val endpoint = f"$baseURL/$appName/$subnetEndpoint/$id/$addressEndpoint"
    sendRequest(endpoint, method="GET")
  }

  /**
   * Send a request to the api to retrieve a specific address with unique `id`.
   *
   * @param id The unique ID of the address
   * @return A tuple containing the status code and the returned json
   */
  def getAddress(id: String): (Int, String) = {
    val endpoint = f"$baseURL/$appName/$addressEndpoint/$id"
    sendRequest(endpoint, method="GET")
  }
}
