package api.objects

import api.Client
import play.api.libs.json.{Json, Reads}

case class Address(
                  id: String,
                  subnetId: String,
                  ip: String,
                  is_gateway: Option[String],
                  description: Option[String],
                  hostname: Option[String],
                  mac: Option[String],
                  deviceId: Option[String],
                  customer_id: Option[String]
                  ){
  // The API returns a string (0/1) OR null.
  val is_gateway_bool: Boolean = is_gateway match {
    case Some("1") => true
    case _ => false
  }
}

/**
 * The companion object exposing methods to operate with addresses and the
 * API.
 */
object Address extends APIObject[Address] {

  override protected implicit val decoder: Reads[Address] = Json.reads[Address]


  /**
   * Retrieve the address with unique `id`.
   *
   * @param id The unique ID of the address
   * @return An `Address` object
   */
  def getAddress(id: Int): Address = getAddress(id.toString)
  def getAddress(id: String): Address = {
    val (_, json) = Client.getAddress(id)
    extractFromJson(json)
  }

}
