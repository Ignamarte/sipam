package api.objects.collections

import api.Client
import api.objects.Section
import play.api.libs.json.{Json, Reads}

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * An object to retrieve a list of section.
 */
object Sections extends APIObjectCollection[Section] {

  override protected implicit val decoder: Reads[Section] = Json.reads[Section]

  /**
   * Get all sections from the API.
   *
   * @return A list of `Section`
   */
  def getSections: Future[List[Section]] = Future {
    val (_, json) = Client.getSections
    extractFromJson(json)
  }
}
