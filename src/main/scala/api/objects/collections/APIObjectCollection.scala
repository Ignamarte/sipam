package api.objects.collections

import errors.APIError
import play.api.libs.json.{JsError, JsSuccess, Json, Reads}

/**
 * A list of generic object returned by the phpipam API.
 * Implementations must implement the Json decoder.
 */
trait APIObjectCollection[A] {

  // Objects are wrapped in a data object by the phpipam API.
  case class Wrapper(data: Option[List[A]])

  protected implicit val decoder: Reads[A]

  /**
   * Converts json returned by the api to a list of generic type defined.
   *
   * @param jsonString The json returned by the API.
   * @return A new list of instances of the generic object.
   */
  def extractFromJson(jsonString: String): List[A] = {
    implicit val format: Reads[Wrapper] = Json.reads[Wrapper]
    val json = Json.parse(jsonString)

    Json.fromJson[Wrapper](json) match {
      case JsSuccess(r, _) => r.data.getOrElse(List[A]())
      case e: JsError => throw APIError(422, f"Errors: ${JsError toJson e}")
    }
  }
}
