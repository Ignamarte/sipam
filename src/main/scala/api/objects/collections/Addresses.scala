package api.objects.collections

import api.Client
import api.objects.Address
import play.api.libs.json.{Json, Reads}

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * An object to retrieve a list of addresses.
 */
object Addresses extends APIObjectCollection[Address]{

  override protected implicit val decoder: Reads[Address] = Json.reads[Address]

  /**
   * Get all addresses of a subnet from the API.
   *
   * @return A list of `Address`
   */
  def getSubnetAddresses(id: Int): Future[List[Address]] = getSubnetAddresses(id.toString)
  def getSubnetAddresses(id: String): Future[List[Address]] = Future {
    val (_, json) = Client.getAddressesFromSubnet(id)
    extractFromJson(json)
  }
}
