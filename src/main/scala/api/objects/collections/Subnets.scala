package api.objects.collections

import api.Client
import api.objects.Subnet
import play.api.libs.json.{Json, Reads}

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * An object to retrieve a list of subnet.
 */
object Subnets extends APIObjectCollection[Subnet] {

  override protected implicit val decoder: Reads[Subnet] = Json.reads[Subnet]

  /**
   * Get all subnets of a section from the API.
   *
   * @return A list of `Section`.
   */
  def getSectionSubnets(id: Int): Future[List[Subnet]] = getSectionSubnets(id.toString)
  def getSectionSubnets(id: String): Future[List[Subnet]] = Future {
    val (_, json) = Client.getSubnetsFromSection(id)
    extractFromJson(json)
  }
}
