package api.objects

import api.Client
import org.apache.commons.net.util.SubnetUtils
import play.api.libs.json.{Json, Reads}
import utils.types.Ipv4

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global


/**
 * A single PHP Ipam subnet.
 *
 * @param id The unique ID of the subnet
 * @param subnet The IP address of the subnet
 * @param mask The netmask of the subnet
 * @param sectionId The parent section ID
 * @param description The description of the subnet
 * @param masterSubnetId The parent subnet ID
 * @param vlanId The vlan ID of the subnet
 */
case class Subnet(
                  id: String,
                  subnet: String,
                  mask: String,
                  sectionId: String,
                  description: Option[String],
                  masterSubnetId: String,
                  vlanId: Option[String],
                  ) {
  private lazy val subnetUtil = new SubnetUtils(f"$subnet/$mask")
  lazy val lowAddresses: Ipv4 = Ipv4.fromString(subnetUtil.getInfo.getLowAddress)
  lazy val highAddresses: Ipv4 = Ipv4.fromString(subnetUtil.getInfo.getHighAddress)
  lazy val allAddresses: Array[Ipv4] = subnetUtil.getInfo.getAllAddresses.map(Ipv4.fromString)
}


/**
 * The companion object exposing methods to operate with subnets and the
 * API.
 */
object Subnet extends APIObject[Subnet] {
  override implicit val decoder: Reads[Subnet] = Json.reads[Subnet]


  /**
   * Retrieve the subnet with unique `id`.
   *
   * @param id The unique ID of the subnet
   * @return A `Subnet` object
   */
  def getSubnet(id: Int): Future [Subnet] = getSubnet(id.toString)
  def getSubnet(id: String): Future[Subnet] = Future {
    val (_, json) = Client.getSubnet(id)
    extractFromJson(json)
  }
}

