package api.objects

import play.api.libs.json.{Json, Reads}


/**
 * A single php ipam section.
 *
 * @param id The unique ID of the section
 * @param name The name of the section
 * @param description The description of the section
 * @param masterSection The parent section
 * @param DNS Main DNS for this section
 */
case class Section(
                  id: String,
                  name: String,
                  description: Option[String],
                  masterSection: String,
                  DNS: Option[String]
                  )

/**
 * The companion object exposing methods to operate with auth tokens and the
 * API.
 */
object Section extends APIObject[Section] {
  override protected implicit val decoder: Reads[Section] = Json.reads[Section]

  /**
   * Get the section with unique `id`.
   *
   * @param id The unique ID of the section
   * @return A `Section` object
   */
  def getSection(id: Int): Section = getSection(id.toString)
  def getSection(id: String): Section = {
    val (_, json) = api.Client.getSection(id)
    extractFromJson(json)
  }
}


