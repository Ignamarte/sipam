package api.objects

import errors.APIError
import play.api.libs.json.{JsError, JsSuccess, Json, Reads}

/**
 * Generic object returned by the phpipam API.
 * Implementations must implement the Json decoder.
 */
trait APIObject[A] {

  // Objects are wrapped in a data object by the phpipam API.
  case class Wrapper(data: A)

  protected implicit val decoder: Reads[A]

  /**
   * Converts json returned by the api to the generic type defined.
   *
   * @param jsonString The json returned by the API.
   * @return A new instance of the generic object.
   */
  def extractFromJson(jsonString: String): A = {
    implicit val format: Reads[Wrapper] = Json.reads[Wrapper]
    val json = Json.parse(jsonString)

    Json.fromJson[Wrapper](json) match {
      case JsSuccess(r, _) => r.data
      case e: JsError => throw APIError(422, f"Errors: ${JsError toJson e}")
    }
  }
}
