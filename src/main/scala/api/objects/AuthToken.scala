package api.objects

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import play.api.libs.json.{Json, Reads}

/**
 * An object containing the auth token for the API.
 *
 * @param token An auth token provided by the API
 * @param expires The date at which the token is revoked
 */
case class AuthToken(token: String, expires: String) {
  def expirationDate: LocalDateTime = LocalDateTime.parse(
    expires,
    DateTimeFormatter.ofPattern("uuuu-MM-DD HH:mm:ss")
  )
}

/**
 * The companion object exposing methods to operate with auth tokens and the
 * API.
 */
object AuthToken extends APIObject[AuthToken] {
  override protected implicit val decoder: Reads[AuthToken] = Json
    .reads[AuthToken]

  /**
   * Retrieve a user token from the api.
   *
   * @param user An existing username
   * @param pass The password matching the username
   * @return An `AuthToken` object
   */
  def getAuthToken(user: String, pass: String): AuthToken = {
    val (_, json) = api.Client.requestAuthToken(user, pass)
    extractFromJson(json)
  }
}
