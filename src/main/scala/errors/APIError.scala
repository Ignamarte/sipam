package errors

case class APIError (statusCode:Int, message: String = "")
  extends Exception(s"[Error Code : $statusCode] $message")
