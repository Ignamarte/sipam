name := "sipam"

version := "0.1"

scalaVersion := "2.13.1"

/* Dependencies */
resolvers += Resolver.jcenterRepo

libraryDependencies += "commons-net" % "commons-net" % "3.6"
libraryDependencies += "com.typesafe.play" %% "play-json" % "2.8.1"
libraryDependencies += "com.github.pureconfig" %% "pureconfig" % "0.12.2"
libraryDependencies += "com.lihaoyi" %% "requests" % "0.4.6"

/* JAVAFX */
fork := true

libraryDependencies += "org.scalafx" %% "scalafx" % "12.0.2-R18"
libraryDependencies += "com.panemu" % "tiwulfx" % "3.0"
libraryDependencies += "io.github.typhon0" % "AnimateFX" % "1.2.1"
libraryDependencies += "de.jensd" % "fontawesomefx-commons" % "11.0"

// Determine OS version of JavaFX binaries
lazy val osName = System.getProperty("os.name") match {
  case n if n.startsWith("Linux")   => "linux"
  case n if n.startsWith("Mac")     => "mac"
  case n if n.startsWith("Windows") => "win"
  case _ => throw new Exception("Unknown platform!")
}

// Add dependency on JavaFX libraries, OS dependent
lazy val javaFXModules = Seq("base", "controls", "fxml", "graphics", "media", "swing", "web")
libraryDependencies ++= javaFXModules.map( m =>
  "org.openjfx" % s"javafx-$m" % "12.0.2" classifier osName
)

/* PACKAGING */
enablePlugins(JavaAppPackaging)
enablePlugins(UniversalPlugin)
enablePlugins(WindowsPlugin)

// Native Packager
maintainer := "Ignamarte"
packageSummary := "Sipam"
packageDescription := """Simple PHP Ipam client"""

// disable using the Scala version in output paths and artifacts
crossPaths := false
assemblyJarName in assembly := "Sipam.jar"

